# Poster Template

Template for an A0 sized poster based on the *beamerposter* package.

By default the poster uses a two column layout with sub blocks. 
But that is free to be changed (setting for three column layout
is included).

Needs to be compiled with `pdflatex` or `lualatex`.

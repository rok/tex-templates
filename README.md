# LaTeX Templates

LaTeX templates for the MPI MiS.

## Presentation

Presentation using LaTeX Beamer.

## Poster

Vertical A0 poster template.

## Letter

Template for official MPI MiS letters.
